import java.time.LocalDateTime;
import java.util.Objects;

public class Human {
    private int age;
    private String firstName;
    private String lastName;
    private String foreName;
    private char sex;
    private LocalDateTime dateOfBirth;

    public int getAge() {
        return age;
    }

    public String getLastName() {
        return lastName;
    }

    public Human(int age, String firstName, String lastName, String foreName, char sex, LocalDateTime dateOfBirth) {
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.foreName = foreName;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                sex == human.sex &&
                firstName.equals(human.firstName) &&
                lastName.equals(human.lastName) &&
                Objects.equals(foreName, human.foreName) &&
                dateOfBirth.equals(human.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, firstName, lastName, foreName, sex, dateOfBirth);
    }

    @Override
    public String toString() {
        return "Human{" +
                "age=" + age +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", foreName='" + foreName + '\'' +
                ", sex=" + sex +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
