import java.util.List;

public interface HumanService {
    void SeeFirstAlphabetAndLessThan20(List<Human> humans, String[] allowedLetters);
     boolean checkHuman(List<Human> humans, Human template);
}
