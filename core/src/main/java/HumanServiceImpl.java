import java.util.List;

public class HumanServiceImpl implements HumanService {

    private static HumanServiceImpl instance = new HumanServiceImpl();
    public static HumanServiceImpl getInstance() {
        return instance;
    }

    private String[] allowedLetters = new String[5];

    @Override
    public boolean checkHuman(List<Human> humans, Human template) {
        return humans.contains(template);
    }

    @Override
    public void SeeFirstAlphabetAndLessThan20(List<Human> humans, String[] allowedLetters) {

        for (Human human : humans) {
            if (human.getAge() < 20) {
                for (int i = 0; i < allowedLetters.length; i++) {
                    if (human.getLastName().startsWith(allowedLetters[i].toUpperCase()) ||
                            human.getLastName().startsWith(allowedLetters[i].toLowerCase())) {
                        System.out.println(human);

                    }
                }
            }
        }
    }
}
