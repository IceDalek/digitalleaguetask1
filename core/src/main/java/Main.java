import java.time.LocalDateTime;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String[] allowedLetters = new String[5];
        allowedLetters[0] = "а";
        allowedLetters[1] = "б";
        allowedLetters[2] = "в";
        allowedLetters[3] = "г";
        allowedLetters[4] = "д";
        Human human = new Human(20, "Denis", "Кефиров",
                "Nikolaevich", 'M', LocalDateTime.of(2001, 4, 4, 0, 0, 0));
        Human human1 = new Human(15, "Nikita", "Бобов",
                "Nikolaevich", 'M', LocalDateTime.of(2002, 4, 4, 0, 0, 0));
        Human human2 = new Human(19, "Nikita", "Аликов",
                "Nikolaevich", 'M', LocalDateTime.of(2002, 4, 4, 0, 0, 0));

        ArrayList<Human> humans = new ArrayList<>();
        humans.add(human);
        humans.add(human1);
        humans.add(human2);
        HumanServiceImpl humanHelper = HumanServiceImpl.getInstance();
        System.out.println(humanHelper.checkHuman(humans, human2));
        humanHelper.SeeFirstAlphabetAndLessThan20(humans, allowedLetters);
    }
}
